﻿namespace AppLourde
{
    partial class AccueilForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Button PrecedentButton;
            DeconnexionButton = new Button();
            AjoutTabPage = new TabPage();
            addPersonButton = new Button();
            lifeStatusLabel = new Label();
            lifeStatusComboBox = new ComboBox();
            cinTextBox = new TextBox();
            idcardLabel = new Label();
            genderGroupBox = new GroupBox();
            girlRadioButton = new RadioButton();
            boyRadioButton = new RadioButton();
            birthdayLabel = new Label();
            birhtdayDateTimePicker = new DateTimePicker();
            firstnameTextBox = new TextBox();
            firstnameLabel = new Label();
            nameTextBox = new TextBox();
            nameLabel = new Label();
            Ajoutlabel = new Label();
            ListeTabPage = new TabPage();
            SuivantButton = new Button();
            ListeDataGridView = new DataGridView();
            Death = new DataGridViewButtonColumn();
            tabControl1 = new TabControl();
            PrecedentButton = new Button();
            AjoutTabPage.SuspendLayout();
            genderGroupBox.SuspendLayout();
            ListeTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)ListeDataGridView).BeginInit();
            tabControl1.SuspendLayout();
            SuspendLayout();
            // 
            // PrecedentButton
            // 
            PrecedentButton.BackColor = SystemColors.MenuHighlight;
            PrecedentButton.ForeColor = SystemColors.ButtonHighlight;
            PrecedentButton.Location = new Point(97, 637);
            PrecedentButton.Margin = new Padding(3, 4, 3, 4);
            PrecedentButton.Name = "PrecedentButton";
            PrecedentButton.Size = new Size(162, 57);
            PrecedentButton.TabIndex = 1;
            PrecedentButton.Text = "Precedent";
            PrecedentButton.UseVisualStyleBackColor = false;
            // 
            // DeconnexionButton
            // 
            DeconnexionButton.BackColor = SystemColors.MenuHighlight;
            DeconnexionButton.ForeColor = SystemColors.ButtonHighlight;
            DeconnexionButton.Location = new Point(32, 12);
            DeconnexionButton.Margin = new Padding(3, 4, 3, 4);
            DeconnexionButton.Name = "DeconnexionButton";
            DeconnexionButton.Size = new Size(126, 31);
            DeconnexionButton.TabIndex = 1;
            DeconnexionButton.Text = "Deconnexion";
            DeconnexionButton.UseVisualStyleBackColor = false;
            DeconnexionButton.Click += DeconnexionButton_Click;
            // 
            // AjoutTabPage
            // 
            AjoutTabPage.Controls.Add(addPersonButton);
            AjoutTabPage.Controls.Add(lifeStatusLabel);
            AjoutTabPage.Controls.Add(lifeStatusComboBox);
            AjoutTabPage.Controls.Add(cinTextBox);
            AjoutTabPage.Controls.Add(idcardLabel);
            AjoutTabPage.Controls.Add(genderGroupBox);
            AjoutTabPage.Controls.Add(birthdayLabel);
            AjoutTabPage.Controls.Add(birhtdayDateTimePicker);
            AjoutTabPage.Controls.Add(firstnameTextBox);
            AjoutTabPage.Controls.Add(firstnameLabel);
            AjoutTabPage.Controls.Add(nameTextBox);
            AjoutTabPage.Controls.Add(nameLabel);
            AjoutTabPage.Controls.Add(Ajoutlabel);
            AjoutTabPage.Location = new Point(4, 29);
            AjoutTabPage.Margin = new Padding(3, 4, 3, 4);
            AjoutTabPage.Name = "AjoutTabPage";
            AjoutTabPage.Padding = new Padding(3, 4, 3, 4);
            AjoutTabPage.Size = new Size(1193, 726);
            AjoutTabPage.TabIndex = 1;
            AjoutTabPage.Text = "Ajouter nouvelle personne";
            AjoutTabPage.UseVisualStyleBackColor = true;
            // 
            // addPersonButton
            // 
            addPersonButton.BackColor = SystemColors.Highlight;
            addPersonButton.ForeColor = SystemColors.ButtonHighlight;
            addPersonButton.Location = new Point(514, 517);
            addPersonButton.Margin = new Padding(3, 4, 3, 4);
            addPersonButton.Name = "addPersonButton";
            addPersonButton.Size = new Size(174, 73);
            addPersonButton.TabIndex = 13;
            addPersonButton.Text = "Add person";
            addPersonButton.UseVisualStyleBackColor = false;
            addPersonButton.Click += addPersonButton_Click;
            // 
            // lifeStatusLabel
            // 
            lifeStatusLabel.AutoSize = true;
            lifeStatusLabel.Location = new Point(266, 435);
            lifeStatusLabel.Name = "lifeStatusLabel";
            lifeStatusLabel.Size = new Size(77, 20);
            lifeStatusLabel.TabIndex = 12;
            lifeStatusLabel.Text = "Life Status";
            // 
            // lifeStatusComboBox
            // 
            lifeStatusComboBox.FormattingEnabled = true;
            lifeStatusComboBox.Location = new Point(407, 424);
            lifeStatusComboBox.Margin = new Padding(3, 4, 3, 4);
            lifeStatusComboBox.Name = "lifeStatusComboBox";
            lifeStatusComboBox.Size = new Size(474, 28);
            lifeStatusComboBox.TabIndex = 11;
            lifeStatusComboBox.Text = "Single";
            // 
            // cinTextBox
            // 
            cinTextBox.Location = new Point(407, 365);
            cinTextBox.Margin = new Padding(3, 4, 3, 4);
            cinTextBox.Name = "cinTextBox";
            cinTextBox.Size = new Size(474, 27);
            cinTextBox.TabIndex = 10;
            // 
            // idcardLabel
            // 
            idcardLabel.AutoSize = true;
            idcardLabel.Location = new Point(266, 376);
            idcardLabel.Name = "idcardLabel";
            idcardLabel.Size = new Size(33, 20);
            idcardLabel.TabIndex = 9;
            idcardLabel.Text = "CIN";
            // 
            // genderGroupBox
            // 
            genderGroupBox.Controls.Add(girlRadioButton);
            genderGroupBox.Controls.Add(boyRadioButton);
            genderGroupBox.Location = new Point(266, 285);
            genderGroupBox.Margin = new Padding(3, 4, 3, 4);
            genderGroupBox.Name = "genderGroupBox";
            genderGroupBox.Padding = new Padding(3, 4, 3, 4);
            genderGroupBox.Size = new Size(615, 73);
            genderGroupBox.TabIndex = 8;
            genderGroupBox.TabStop = false;
            genderGroupBox.Text = "Gender";
            // 
            // girlRadioButton
            // 
            girlRadioButton.AutoSize = true;
            girlRadioButton.Location = new Point(391, 29);
            girlRadioButton.Margin = new Padding(3, 4, 3, 4);
            girlRadioButton.Name = "girlRadioButton";
            girlRadioButton.Size = new Size(53, 24);
            girlRadioButton.TabIndex = 1;
            girlRadioButton.Text = "Girl";
            girlRadioButton.UseVisualStyleBackColor = true;
            girlRadioButton.CheckedChanged += girlRadioButton_CheckedChanged;
            // 
            // boyRadioButton
            // 
            boyRadioButton.AutoSize = true;
            boyRadioButton.Checked = true;
            boyRadioButton.Location = new Point(141, 29);
            boyRadioButton.Margin = new Padding(3, 4, 3, 4);
            boyRadioButton.Name = "boyRadioButton";
            boyRadioButton.Size = new Size(55, 24);
            boyRadioButton.TabIndex = 0;
            boyRadioButton.TabStop = true;
            boyRadioButton.Text = "Boy";
            boyRadioButton.UseVisualStyleBackColor = true;
            boyRadioButton.CheckedChanged += boyRadioButton_CheckedChanged;
            // 
            // birthdayLabel
            // 
            birthdayLabel.AutoSize = true;
            birthdayLabel.Location = new Point(266, 236);
            birthdayLabel.Name = "birthdayLabel";
            birthdayLabel.Size = new Size(64, 20);
            birthdayLabel.TabIndex = 6;
            birthdayLabel.Text = "Birthday";
            // 
            // birhtdayDateTimePicker
            // 
            birhtdayDateTimePicker.Location = new Point(407, 228);
            birhtdayDateTimePicker.Margin = new Padding(3, 4, 3, 4);
            birhtdayDateTimePicker.Name = "birhtdayDateTimePicker";
            birhtdayDateTimePicker.Size = new Size(474, 27);
            birhtdayDateTimePicker.TabIndex = 5;
            // 
            // firstnameTextBox
            // 
            firstnameTextBox.Location = new Point(407, 173);
            firstnameTextBox.Margin = new Padding(3, 4, 3, 4);
            firstnameTextBox.Name = "firstnameTextBox";
            firstnameTextBox.Size = new Size(474, 27);
            firstnameTextBox.TabIndex = 4;
            // 
            // firstnameLabel
            // 
            firstnameLabel.AutoSize = true;
            firstnameLabel.Location = new Point(266, 184);
            firstnameLabel.Name = "firstnameLabel";
            firstnameLabel.Size = new Size(73, 20);
            firstnameLabel.TabIndex = 3;
            firstnameLabel.Text = "Firstname";
            // 
            // nameTextBox
            // 
            nameTextBox.Location = new Point(407, 123);
            nameTextBox.Margin = new Padding(3, 4, 3, 4);
            nameTextBox.Name = "nameTextBox";
            nameTextBox.Size = new Size(474, 27);
            nameTextBox.TabIndex = 2;
            // 
            // nameLabel
            // 
            nameLabel.AutoSize = true;
            nameLabel.Location = new Point(266, 133);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new Size(49, 20);
            nameLabel.TabIndex = 1;
            nameLabel.Text = "Name";
            // 
            // Ajoutlabel
            // 
            Ajoutlabel.AutoSize = true;
            Ajoutlabel.Font = new Font("PMingLiU-ExtB", 36F, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point);
            Ajoutlabel.ForeColor = SystemColors.MenuHighlight;
            Ajoutlabel.Location = new Point(407, 27);
            Ajoutlabel.Name = "Ajoutlabel";
            Ajoutlabel.Size = new Size(412, 60);
            Ajoutlabel.TabIndex = 0;
            Ajoutlabel.Text = "Add new person";
            // 
            // ListeTabPage
            // 
            ListeTabPage.Controls.Add(SuivantButton);
            ListeTabPage.Controls.Add(PrecedentButton);
            ListeTabPage.Controls.Add(ListeDataGridView);
            ListeTabPage.Location = new Point(4, 29);
            ListeTabPage.Margin = new Padding(3, 4, 3, 4);
            ListeTabPage.Name = "ListeTabPage";
            ListeTabPage.Padding = new Padding(3, 4, 3, 4);
            ListeTabPage.Size = new Size(1193, 726);
            ListeTabPage.TabIndex = 0;
            ListeTabPage.Text = "Liste des personnes";
            ListeTabPage.UseVisualStyleBackColor = true;
            // 
            // SuivantButton
            // 
            SuivantButton.BackColor = SystemColors.MenuHighlight;
            SuivantButton.ForeColor = SystemColors.ButtonHighlight;
            SuivantButton.Location = new Point(912, 637);
            SuivantButton.Margin = new Padding(3, 4, 3, 4);
            SuivantButton.Name = "SuivantButton";
            SuivantButton.Size = new Size(161, 57);
            SuivantButton.TabIndex = 2;
            SuivantButton.Text = "Suivant";
            SuivantButton.UseVisualStyleBackColor = false;
            // 
            // ListeDataGridView
            // 
            ListeDataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            ListeDataGridView.Columns.AddRange(new DataGridViewColumn[] { Death });
            ListeDataGridView.Location = new Point(19, 23);
            ListeDataGridView.Margin = new Padding(3, 4, 3, 4);
            ListeDataGridView.Name = "ListeDataGridView";
            ListeDataGridView.RowHeadersWidth = 51;
            ListeDataGridView.RowTemplate.Height = 25;
            ListeDataGridView.Size = new Size(1151, 607);
            ListeDataGridView.TabIndex = 0;
            ListeDataGridView.CellContentClick += ListeDataGridView_CellContentClick;
            // 
            // Death
            // 
            Death.HeaderText = "Death";
            Death.MinimumWidth = 6;
            Death.Name = "Death";
            Death.Text = "Death";
            Death.UseColumnTextForButtonValue = true;
            Death.Width = 125;
            // 
            // tabControl1
            // 
            tabControl1.Controls.Add(ListeTabPage);
            tabControl1.Controls.Add(AjoutTabPage);
            tabControl1.Location = new Point(32, 65);
            tabControl1.Margin = new Padding(3, 4, 3, 4);
            tabControl1.Name = "tabControl1";
            tabControl1.SelectedIndex = 0;
            tabControl1.Size = new Size(1201, 759);
            tabControl1.TabIndex = 2;
            // 
            // AccueilForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoSize = true;
            ClientSize = new Size(1263, 853);
            Controls.Add(tabControl1);
            Controls.Add(DeconnexionButton);
            Margin = new Padding(3, 4, 3, 4);
            Name = "AccueilForm";
            Text = "Accueil";
            Load += Accueil_Load;
            AjoutTabPage.ResumeLayout(false);
            AjoutTabPage.PerformLayout();
            genderGroupBox.ResumeLayout(false);
            genderGroupBox.PerformLayout();
            ListeTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)ListeDataGridView).EndInit();
            tabControl1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private Button DeconnexionButton;
        private TabPage AjoutTabPage;
        private Label Ajoutlabel;
        private TabPage ListeTabPage;
        private Button SuivantButton;
        private DataGridView ListeDataGridView;
        private TabControl tabControl1;
        private TextBox nameTextBox;
        private Label nameLabel;
        private TextBox firstnameTextBox;
        private Label firstnameLabel;
        private Label birthdayLabel;
        private DateTimePicker birhtdayDateTimePicker;
        private GroupBox genderGroupBox;
        private RadioButton boyRadioButton;
        private RadioButton girlRadioButton;
        private Label idcardLabel;
        private TextBox cinTextBox;
        private Label lifeStatusLabel;
        private ComboBox lifeStatusComboBox;
        private Button addPersonButton;
        private DataGridViewButtonColumn Death;
    }
}
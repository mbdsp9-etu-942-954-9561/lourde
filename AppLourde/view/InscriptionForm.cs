﻿using AppLourde.controller;
using AppLourde.model;
using AppLourde.service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppLourde
{
    public partial class InscriptionForm : Form
    {

        AdminLourdController adminLourdController { get; set; }


        public InscriptionForm()
        {
            InitializeComponent();
            adminLourdController = AdminLourdController.getInstance();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }


        private async void InscriprionButton_Click(object sender, EventArgs e)
        {
            string username = textBox1.Text;
            string password = textBox2.Text;

            AdminLourd adminLourd = new AdminLourd(username, password);

            try
            {
                AdminLourd admin = await adminLourdController.inscription(adminLourd);
                Client.user = admin;
                var home = await AccueilForm.getInstance();
                home.Show();
                this.Hide();
            } catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            


        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Ouvrir la fenêtre d'inscription.
            var fenetreConnexion = new ConnexionForm();
            fenetreConnexion.Show();
            this.Hide(); // Vous pouvez cacher la fenêtre de connexion si vous le souhaitez.
        }

        private void InscriptionForm_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿namespace AppLourde
{
    partial class InscriptionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            InscriptionLabel = new Label();
            UsernameLabel = new Label();
            PasswordLabel = new Label();
            textBox1 = new TextBox();
            textBox2 = new TextBox();
            InscriprionButton = new Button();
            SeconnecterLinkLabel = new LinkLabel();
            SuspendLayout();
            // 
            // InscriptionLabel
            // 
            InscriptionLabel.AutoSize = true;
            InscriptionLabel.Font = new Font("PMingLiU-ExtB", 36F, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point);
            InscriptionLabel.ForeColor = SystemColors.MenuHighlight;
            InscriptionLabel.Location = new Point(451, 79);
            InscriptionLabel.Name = "InscriptionLabel";
            InscriptionLabel.Size = new Size(281, 60);
            InscriptionLabel.TabIndex = 0;
            InscriptionLabel.Text = "Inscription";
            InscriptionLabel.Click += label1_Click;
            // 
            // UsernameLabel
            // 
            UsernameLabel.AutoSize = true;
            UsernameLabel.Location = new Point(326, 216);
            UsernameLabel.Name = "UsernameLabel";
            UsernameLabel.Size = new Size(75, 20);
            UsernameLabel.TabIndex = 1;
            UsernameLabel.Text = "Username";
            // 
            // PasswordLabel
            // 
            PasswordLabel.AutoSize = true;
            PasswordLabel.Location = new Point(326, 321);
            PasswordLabel.Name = "PasswordLabel";
            PasswordLabel.Size = new Size(70, 20);
            PasswordLabel.TabIndex = 2;
            PasswordLabel.Text = "Password";
            // 
            // textBox1
            // 
            textBox1.Location = new Point(451, 212);
            textBox1.Margin = new Padding(3, 4, 3, 4);
            textBox1.Name = "textBox1";
            textBox1.Size = new Size(342, 27);
            textBox1.TabIndex = 4;
            // 
            // textBox2
            // 
            textBox2.Location = new Point(451, 317);
            textBox2.Margin = new Padding(3, 4, 3, 4);
            textBox2.Name = "textBox2";
            textBox2.Size = new Size(339, 27);
            textBox2.TabIndex = 5;
            textBox2.TextChanged += textBox2_TextChanged;
            // 
            // InscriprionButton
            // 
            InscriprionButton.BackColor = SystemColors.MenuHighlight;
            InscriprionButton.ForeColor = SystemColors.ButtonFace;
            InscriprionButton.Location = new Point(520, 385);
            InscriprionButton.Margin = new Padding(3, 4, 3, 4);
            InscriprionButton.Name = "InscriprionButton";
            InscriprionButton.Size = new Size(159, 78);
            InscriprionButton.TabIndex = 7;
            InscriprionButton.Text = "S'inscrire";
            InscriprionButton.UseVisualStyleBackColor = false;
            InscriprionButton.Click += InscriprionButton_Click;
            // 
            // SeconnecterLinkLabel
            // 
            SeconnecterLinkLabel.AutoSize = true;
            SeconnecterLinkLabel.Location = new Point(550, 489);
            SeconnecterLinkLabel.Name = "SeconnecterLinkLabel";
            SeconnecterLinkLabel.Size = new Size(94, 20);
            SeconnecterLinkLabel.TabIndex = 8;
            SeconnecterLinkLabel.TabStop = true;
            SeconnecterLinkLabel.Text = "Se connecter";
            SeconnecterLinkLabel.LinkClicked += linkLabel1_LinkClicked;
            // 
            // InscriptionForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1176, 574);
            Controls.Add(SeconnecterLinkLabel);
            Controls.Add(InscriprionButton);
            Controls.Add(textBox2);
            Controls.Add(textBox1);
            Controls.Add(PasswordLabel);
            Controls.Add(UsernameLabel);
            Controls.Add(InscriptionLabel);
            Margin = new Padding(3, 4, 3, 4);
            Name = "InscriptionForm";
            Text = "InscriptionForm";
            Load += InscriptionForm_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label InscriptionLabel;
        private Label UsernameLabel;
        private Label PasswordLabel;
        private TextBox textBox1;
        private TextBox textBox2;
        private Button InscriprionButton;
        private LinkLabel SeconnecterLinkLabel;
    }
}
﻿namespace AppLourde.view
{
    partial class DeathForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            deathLabel = new Label();
            deathDateTimePicker = new DateTimePicker();
            deathPersonButton = new Button();
            SuspendLayout();
            // 
            // deathLabel
            // 
            deathLabel.AutoSize = true;
            deathLabel.Location = new Point(70, 111);
            deathLabel.Name = "deathLabel";
            deathLabel.Size = new Size(85, 20);
            deathLabel.TabIndex = 8;
            deathLabel.Text = "Death Date\r\n";
            // 
            // deathDateTimePicker
            // 
            deathDateTimePicker.Location = new Point(211, 103);
            deathDateTimePicker.Margin = new Padding(3, 4, 3, 4);
            deathDateTimePicker.Name = "deathDateTimePicker";
            deathDateTimePicker.Size = new Size(362, 27);
            deathDateTimePicker.TabIndex = 7;
            // 
            // deathPersonButton
            // 
            deathPersonButton.BackColor = SystemColors.Highlight;
            deathPersonButton.ForeColor = SystemColors.ButtonHighlight;
            deathPersonButton.Location = new Point(237, 181);
            deathPersonButton.Margin = new Padding(3, 4, 3, 4);
            deathPersonButton.Name = "deathPersonButton";
            deathPersonButton.Size = new Size(138, 73);
            deathPersonButton.TabIndex = 14;
            deathPersonButton.Text = "person dead\r\n";
            deathPersonButton.UseVisualStyleBackColor = false;
            deathPersonButton.Click += addPersonButton_Click;
            // 
            // DeathForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(652, 332);
            Controls.Add(deathPersonButton);
            Controls.Add(deathLabel);
            Controls.Add(deathDateTimePicker);
            Name = "DeathForm";
            Text = "DeathForm";
            Load += DeathForm_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label deathLabel;
        private DateTimePicker deathDateTimePicker;
        private Button deathPersonButton;
    }
}
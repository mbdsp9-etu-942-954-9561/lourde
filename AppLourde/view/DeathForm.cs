﻿using AppLourde.controller;
using AppLourde.model;
using AppLourde.model.help;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppLourde.view
{
    public partial class DeathForm : Form
    {

        private Personne personne;
        private CitoyenController citoyenController;

        public DeathForm(Personne personne)
        {
            InitializeComponent();
            this.personne = personne;
            citoyenController = CitoyenController.getInstance();
        }

        private void DeathForm_Load(object sender, EventArgs e)
        {

        }

        private void Ajoutlabel_Click(object sender, EventArgs e)
        {

        }

        private async void addPersonButton_Click(object sender, EventArgs e)
        {
            try
            {
                personne.deathdate = deathDateTimePicker.Value.Date.ToString("yyyy-MM-dd");
                Citoyen citoyen = await citoyenController.updateAll(personne);
                AccueilForm accueil = await AccueilForm.getInstance();
                accueil.Show();
                this.Hide();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}

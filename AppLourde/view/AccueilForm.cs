﻿using AppLourde.controller;
using AppLourde.model;
using AppLourde.model.help;
using AppLourde.view;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Runtime.InteropServices.JavaScript.JSType;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace AppLourde
{
    public partial class AccueilForm : Form
    {

        string gender = "Boy";

        private List<Lifestatus> lifestatuses;
        private CitoyenController instance;
        private Paginator<List<Personne>> citoyens;

        private AccueilForm(List<Lifestatus> lifestatuses, Paginator<List<Personne>> citoyens)
        {
            InitializeComponent();
            this.lifestatuses = lifestatuses;
            this.citoyens = citoyens;
            InitializeComboBox();
            instance = CitoyenController.getInstance();
            InitializeDataGrid();
        }

        public static async Task<AccueilForm> getInstance()
        {
            LifeStatusController controller = LifeStatusController.getInstance();
            CitoyenController citoyenController = CitoyenController.getInstance();
            List<Lifestatus> lifestatuses = await controller.read();
            Paginator<List<Personne>> citoyens = await citoyenController.read();
            return new AccueilForm(lifestatuses, citoyens);
        }

        private void InitializeComboBox()
        {
            lifeStatusComboBox.DataSource = lifestatuses;
            lifeStatusComboBox.DisplayMember = "label"; // Display the Name property
            lifeStatusComboBox.ValueMember = "id";     // Use the Id property as the value
        }

        private void InitializeDataGrid()
        {

            DataGridViewTextBoxColumn colId = new DataGridViewTextBoxColumn();
            colId.DataPropertyName = "citizenid";
            colId.HeaderText = "Id";
            ListeDataGridView.Columns.Add(colId);
            DataGridViewTextBoxColumn colName = new DataGridViewTextBoxColumn();
            colName.DataPropertyName = "name";
            colName.HeaderText = "Name";
            ListeDataGridView.Columns.Add(colName);
            DataGridViewTextBoxColumn colFirstname = new DataGridViewTextBoxColumn();
            colFirstname.DataPropertyName = "Firstname";
            colFirstname.HeaderText = "firstname";
            ListeDataGridView.Columns.Add(colFirstname);
            DataGridViewTextBoxColumn colBirthday = new DataGridViewTextBoxColumn();
            colBirthday.DataPropertyName = "birthday";
            colBirthday.HeaderText = "Birthday";
            ListeDataGridView.Columns.Add(colBirthday);
            DataGridViewTextBoxColumn colGender = new DataGridViewTextBoxColumn();
            colGender.DataPropertyName = "gender";
            colGender.HeaderText = "Gender";
            ListeDataGridView.Columns.Add(colGender);
            DataGridViewTextBoxColumn colDeath= new DataGridViewTextBoxColumn();
            colDeath.DataPropertyName = "deathdate";
            colDeath.HeaderText = "DeathDate";
            ListeDataGridView.Columns.Add(colDeath);

            ListeDataGridView.AutoGenerateColumns = false;

            ListeDataGridView.DataSource = citoyens.items;
        }

        private void Accueil_Load(object sender, EventArgs e)
        {

        }

        private void DeconnexionButton_Click(object sender, EventArgs e)
        {
            // Ouvrir la fenêtre d'inscription.
            var fenetreConnexion = new ConnexionForm();
            fenetreConnexion.Show();
            this.Hide(); // Vous pouvez cacher la fenêtre de connexion si vous le souhaitez.
        }

        private void ListeDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (ListeDataGridView.Columns[e.ColumnIndex].Name == "Death")
            {
                // Get the selected row index
                int selectedIndex = e.RowIndex;

                // Get the object bound to the selected row
                Personne selectedObject = ListeDataGridView.Rows[selectedIndex].DataBoundItem as Personne;

                if (selectedObject != null)
                {
                    DeathForm deathForm = new DeathForm(selectedObject);
                    this.Hide();
                    deathForm.Show();
                    
                }
            }
        }

        private async void addPersonButton_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure to add this person?", "Confirmation",
                                         MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                Citoyen citoyen = new Citoyen();
                citoyen.name = nameTextBox.Text;
                citoyen.firstname = firstnameTextBox.Text;

                citoyen.birthday = birhtdayDateTimePicker.Value.Date.ToString("yyyy-MM-dd");

                citoyen.gender = this.gender;

                citoyen.idcard = cinTextBox.Text;
                citoyen.lifeStatus = new Lifestatus(Convert.ToInt16(lifeStatusComboBox.SelectedValue));

                try
                {
                    await instance.createAll(citoyen);
                    citoyens = await instance.read();
                    ListeDataGridView.DataSource = citoyens.items;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }





            }
            else if (result == DialogResult.No)
            {
                // L'utilisateur a choisi "Non", vous pouvez prendre des mesures en conséquence si nécessaire.
            }
        }

        private void girlRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            gender = "Girl";
        }

        private void boyRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            gender = "Boy";
        }

    }
}

﻿namespace AppLourde
{
    partial class ConnexionForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Usernamelabel = new Label();
            UsernametextBox = new TextBox();
            Passwordlabel = new Label();
            PasswordtextBox = new TextBox();
            Connexionbutton = new Button();
            Welcomelabel = new Label();
            InscriptionlinkLabel = new LinkLabel();
            SuspendLayout();
            // 
            // Usernamelabel
            // 
            Usernamelabel.AutoSize = true;
            Usernamelabel.Location = new Point(372, 226);
            Usernamelabel.Name = "Usernamelabel";
            Usernamelabel.Size = new Size(75, 20);
            Usernamelabel.TabIndex = 0;
            Usernamelabel.Text = "Username";
            Usernamelabel.Click += label1_Click;
            // 
            // UsernametextBox
            // 
            UsernametextBox.Location = new Point(479, 222);
            UsernametextBox.Margin = new Padding(3, 4, 3, 4);
            UsernametextBox.Name = "UsernametextBox";
            UsernametextBox.Size = new Size(334, 27);
            UsernametextBox.TabIndex = 1;
            UsernametextBox.TextChanged += UsernametextBox_TextChanged;
            // 
            // Passwordlabel
            // 
            Passwordlabel.AutoSize = true;
            Passwordlabel.Location = new Point(372, 315);
            Passwordlabel.Name = "Passwordlabel";
            Passwordlabel.Size = new Size(70, 20);
            Passwordlabel.TabIndex = 2;
            Passwordlabel.Text = "Password";
            Passwordlabel.Click += label1_Click_1;
            // 
            // PasswordtextBox
            // 
            PasswordtextBox.Location = new Point(479, 311);
            PasswordtextBox.Margin = new Padding(3, 4, 3, 4);
            PasswordtextBox.Name = "PasswordtextBox";
            PasswordtextBox.Size = new Size(334, 27);
            PasswordtextBox.TabIndex = 3;
            PasswordtextBox.TextChanged += textBox1_TextChanged;
            // 
            // Connexionbutton
            // 
            Connexionbutton.BackColor = SystemColors.MenuHighlight;
            Connexionbutton.ForeColor = SystemColors.ButtonHighlight;
            Connexionbutton.Location = new Point(520, 395);
            Connexionbutton.Margin = new Padding(3, 4, 3, 4);
            Connexionbutton.Name = "Connexionbutton";
            Connexionbutton.Size = new Size(193, 84);
            Connexionbutton.TabIndex = 4;
            Connexionbutton.Text = "Connexion";
            Connexionbutton.UseVisualStyleBackColor = false;
            Connexionbutton.Click += Connexionbutton_Click;
            // 
            // Welcomelabel
            // 
            Welcomelabel.AutoSize = true;
            Welcomelabel.Font = new Font("PMingLiU-ExtB", 36F, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point);
            Welcomelabel.ForeColor = SystemColors.MenuHighlight;
            Welcomelabel.Location = new Point(501, 91);
            Welcomelabel.Name = "Welcomelabel";
            Welcomelabel.Size = new Size(248, 60);
            Welcomelabel.TabIndex = 5;
            Welcomelabel.Text = "Welcome";
            Welcomelabel.Click += label1_Click_2;
            // 
            // InscriptionlinkLabel
            // 
            InscriptionlinkLabel.AutoSize = true;
            InscriptionlinkLabel.LinkColor = Color.Blue;
            InscriptionlinkLabel.Location = new Point(581, 516);
            InscriptionlinkLabel.Name = "InscriptionlinkLabel";
            InscriptionlinkLabel.Size = new Size(78, 20);
            InscriptionlinkLabel.TabIndex = 6;
            InscriptionlinkLabel.TabStop = true;
            InscriptionlinkLabel.Text = "Inscription";
            InscriptionlinkLabel.LinkClicked += InscriptionlinkLabel_LinkClicked;
            // 
            // ConnexionForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1213, 597);
            Controls.Add(InscriptionlinkLabel);
            Controls.Add(Welcomelabel);
            Controls.Add(Connexionbutton);
            Controls.Add(PasswordtextBox);
            Controls.Add(Passwordlabel);
            Controls.Add(UsernametextBox);
            Controls.Add(Usernamelabel);
            Margin = new Padding(3, 4, 3, 4);
            Name = "ConnexionForm";
            Text = "Form1";
            Load += Form1_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label Usernamelabel;
        private TextBox UsernametextBox;
        private Label Passwordlabel;
        private TextBox PasswordtextBox;
        private Button Connexionbutton;
        private Label Welcomelabel;
        private LinkLabel InscriptionlinkLabel;
    }
}
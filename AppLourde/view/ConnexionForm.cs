using AppLourde.controller;
using AppLourde.model;
using AppLourde.service;
using System.Net.Http;

namespace AppLourde
{
    public partial class ConnexionForm : Form
    {

        AdminLourdController adminLourdController;

        public ConnexionForm()
        {
            InitializeComponent();
            adminLourdController = AdminLourdController.getInstance();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private async void Connexionbutton_Click(object sender, EventArgs e)
        {
            string username = UsernametextBox.Text;
            string password = PasswordtextBox.Text;

            try
            {
                AdminLourd adminLourd = await adminLourdController.connexion(username, password);
                Client.user = adminLourd;
                var accueil = await AccueilForm.getInstance();
                accueil.Show();
                this.Hide(); // Vous pouvez cacher la fen�tre de connexion si vous le souhaitez.

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void UsernametextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click_2(object sender, EventArgs e)
        {

        }

        private void InscriptionlinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Ouvrir la fen�tre d'inscription.
            var fenetreInscription = new InscriptionForm();
            fenetreInscription.Show();
            this.Hide(); // Vous pouvez cacher la fen�tre de connexion si vous le souhaitez.
        }
    }
}
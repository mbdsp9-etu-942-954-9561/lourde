﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLourde.model.help
{
    internal class Paginator<T>
    {
        public T items { set; get; }
        public Pag paginator{ set; get; }
    }

    internal class Pag
    {
        public int totalItems { set; get; }
        public int offset { set; get; }
        public int itemsPerPage { set; get; }
        public int totalPages { set; get; }
        public int page { set; get; }
        public int pagingCounter { set; get; }
        public bool hasPrevPage { set; get; }
        public bool hasNextPage { set; get; }
        public string prevPage { set; get; }
        public string nextPage { set; get; }
    }
}

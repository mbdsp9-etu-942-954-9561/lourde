﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace AppLourde.model.help
{
    public class Personne
    {
        public string _id { get; set; }

        public int citizenid { get; set; }

        public string name { get; set; }

        public string firstname { get; set; }

        public string birthday { get; set; }

        public string gender { get; set; }

        public string idcard { get; set; }

        public string deathdate { get; set; }

        public Lifestatus lifeStatus { get; set; }

        public string password { get; set; }

        public Personne() { }

        public Personne(Citoyen citoyen)
        {
            this.citizenid = citoyen.id;
            this.name = citoyen.name;
            this.firstname = citoyen.firstname;
            this.birthday = citoyen.birthday;
            this.gender = citoyen.gender;
            this.idcard = citoyen.idcard;
            this.deathdate = citoyen.deathdate;
            this.lifeStatus = citoyen.lifeStatus;
            this.password = citoyen.password;
        }
    }
}

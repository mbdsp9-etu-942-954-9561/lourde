﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLourde.model
{
    internal class AdminLourd
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }

        public AdminLourd() { }

        public AdminLourd(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLourde.model
{
    public class Lifestatus
    {
        public int id { get; set; }
        public string label { get; set; }

        public Lifestatus() { }
        public Lifestatus(int id) { this.id = id; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System.Xml.Linq;
using AppLourde.model.help;

namespace AppLourde.model
{
    public class Citoyen
    {
        public int id { get; set; }

        public string name { get; set; }

        public string firstname { get; set; }

        public string birthday { get; set; }

        public string gender { get; set; }

        public string idcard { get; set; }

        public string deathdate { get; set; }

        public Lifestatus lifeStatus { get; set; }

        public string password { get; set; }

        public Citoyen() { }
        public Citoyen(Personne personne) {
            id = personne.citizenid;
            name = personne.name;
            firstname = personne.firstname;
            birthday = personne.birthday;
            gender = personne.gender;
            idcard = personne.idcard;
            deathdate = personne.deathdate;
            lifeStatus = personne.lifeStatus;
            password = personne.password;
        }

    }
}

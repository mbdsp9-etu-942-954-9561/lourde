﻿using AppLourde.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLourde.service
{
    internal class Client
    {


        public static string getBaseUrl()
        {
            return "http://85.31.234.130/api/";
        }

        public static string getNodeUrl()
        {
            return "https://api-node-eeas.onrender.com/";
        }

        public static AdminLourd user { get; set; }


    }
}

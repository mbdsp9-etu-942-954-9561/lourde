﻿using AppLourde.model;
using AppLourde.model.help;
using AppLourde.service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLourde.controller
{
    internal class CitoyenController
    {
        HttpClient httpClient;
        static CitoyenController instance;

        private CitoyenController()
        {
            httpClient = new HttpClient();
        }

        public static CitoyenController getInstance()
        {
            if (instance == null)
            {
                instance = new CitoyenController();
            }
            return instance;
        }

        public async Task<Paginator<List<Personne>>> read()
        {
            string subUrl = "personne";
            string apiUrl = Client.getNodeUrl() + subUrl;

            HttpResponseMessage response = await httpClient.GetAsync(apiUrl);


            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync();

                

                return JsonConvert.DeserializeObject<Paginator<List<Personne>>>(responseBody); ;

            }
            throw new Exception("Admin Lourde non créer");
        }

        public async Task<Citoyen> CreateRelationnel(Citoyen citoyen)
        {
            string subUrl = "citoyen/create";
            string apiUrl = Client.getBaseUrl() + subUrl;

            string jsonBody = JsonConvert.SerializeObject(citoyen);
            HttpContent content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);

            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync();


                return JsonConvert.DeserializeObject<Citoyen>(responseBody); ;

            }
            throw new Exception("Admin Lourde non créer");
        }

        public async Task<Citoyen> createMongo(Citoyen citoyen)
        {
            string subUrl = "personne";
            string apiUrl = Client.getNodeUrl() + subUrl;

            Personne personne = new Personne(citoyen);
            string jsonBody = JsonConvert.SerializeObject(personne);
            HttpContent content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);


            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<Citoyen>(responseBody); ;

            }
            throw new Exception("Admin Lourde non créer");
        }

        public async Task<Citoyen> createAll(Citoyen citoyen)
        {
            Citoyen citoyen1 = await CreateRelationnel(citoyen);
            Citoyen citoyen2 = await createMongo(citoyen1);
            return citoyen2 ;
        }

        public async Task<Personne> updateMongo(Personne personne)
        {
            string subUrl = "personne/death/" +personne._id.ToString() ;
            string apiUrl = Client.getNodeUrl() + subUrl;

            string jsonBody = JsonConvert.SerializeObject(personne);
            HttpContent content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await httpClient.PutAsync(apiUrl, content);


            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<Personne>(responseBody); ;

            }
            throw new Exception("Personne no update");
        }

        public async Task<Citoyen> updateRelationnel(Personne personne) 
        {
            string subUrl = "citoyen/update/" + personne.citizenid.ToString();
            string apiUrl = Client.getBaseUrl() + subUrl;

            Citoyen citoyen = new Citoyen(personne);
            string jsonBody = JsonConvert.SerializeObject(citoyen);
            HttpContent content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await httpClient.PutAsync(apiUrl, content);


            //if (response.IsSuccessStatusCode)
            //{
                string responseBody = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<Citoyen>(responseBody); ;

        }

        public async Task<Citoyen> updateAll(Personne personne)
        {
            Personne personne1 = await updateMongo(personne);
            Citoyen citoyen2 = await updateRelationnel(personne1);
            return citoyen2;
        }

    }
}

﻿using AppLourde.model;
using AppLourde.service;
using Newtonsoft.Json;
using System.Data;
using System.Text;

namespace AppLourde.controller
{
    internal class AdminLourdController
    {
        HttpClient httpClient;
        static AdminLourdController instance;

        private AdminLourdController()
        {
            httpClient = new HttpClient();
        }

        public static AdminLourdController getInstance() { 
            if (instance == null)
            {
                instance = new AdminLourdController();
            }
            return instance;
        }

        public async Task<AdminLourd> inscription(AdminLourd adminLourd)
        {
            string subUrl = "adminLourd/create";
            string apiUrl = Client.getBaseUrl() + subUrl;

            string jsonBody = JsonConvert.SerializeObject(adminLourd);
            HttpContent content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);


            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<AdminLourd>(responseBody); ;

            }
            throw new Exception("Admin Lourde non créer");
        }

        public async Task<AdminLourd> connexion(string username, string password)
        {
            string subUrl = "adminLourd/login";
            string apiUrl = Client.getBaseUrl() + subUrl;

            AdminLourd admin = new AdminLourd();
            admin.username = username;
            admin.password = password;

            string jsonBody = JsonConvert.SerializeObject(admin);
            Console.WriteLine(jsonBody);
            HttpContent content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);


            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<AdminLourd>(responseBody); ;

            }
            throw new Exception("Erreur");
        }
    }
}

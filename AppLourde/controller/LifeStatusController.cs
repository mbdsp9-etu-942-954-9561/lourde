﻿using AppLourde.model;
using AppLourde.service;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLourde.controller
{
    internal class LifeStatusController
    {
        HttpClient httpClient;
        static LifeStatusController instance;

        private LifeStatusController()
        {
            httpClient = new HttpClient();
        }

        public static LifeStatusController getInstance()
        {
            if (instance == null)
            {
                instance = new LifeStatusController();
            }
            return instance;
        }

        public async Task<List<Lifestatus>> read()
        {
            string subUrl = "lifestatus/read";
            string apiUrl = Client.getBaseUrl() + subUrl;

            HttpResponseMessage response = await httpClient.GetAsync(apiUrl);


            if (response.IsSuccessStatusCode)
            {
                string responseBody = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<List<Lifestatus>>(responseBody); ;

            }
            throw new Exception("Aucun lifestatus");
        }

    }
}
